#!/bin/bash
# This is a helpful script to run on a Dirac node to build
# the final release tarballs from a GIT tag (or branch name).
TOPDIR=`pwd`
DO_TRANS=1
DO_UPCR=1
DO_GASNET=1
DO_TOOLS=1
DIST=distcheck
TRANS_FLAG=" -translator=${TOPDIR}/trans-inst/targ"
REPO_UPCR='git@bitbucket.org:berkeleylab/upc-runtime'
REPO_TRANS='git@bitbucket.org:berkeleylab/upc-translator'
REPO_GASNET='git@bitbucket.org:berkeleylab/gasnet'
UPCR_CONF_FLAGS='--without-multiconf'
GASNET_CONF_FLAGS='--enable-ofi --enable-ucx'
while [ $# -gt 1 ]; do
  case $1 in
    # Append a release candidate ideintifier to tarball names
    -rc*)
      RC=${1#*=} # Use only the text after '=', if present
      ;;
    # Specify non-default repo(s) from which to clone
    -upcr-repo=*) REPO_UPCR=${1#*=} ;;
    -trans-repo=*) REPO_TRANS=${1#*=} ;;
    -gasnet-repo=*) REPO_GASNET=${1#*=} ;;
    # Don't point runtime build at just-built translator
    -net-trans)
      TRANS_FLAG=''
      ;;
    # Don't build a translator tarball (implies -net-trans)
    -no-trans)
      TRANS_FLAG=''
      DO_TRANS='0'
      ;;
    # Don't build runtime tarball
    -no-upcr)
      DO_UPCR='0'
      ;;
    # Don't build GASNet-Tools tarball
    -no-tools)
      DO_TOOLS='0'
      ;;
    # Don't build GASNet tarball (implies -no-tools)
    -no-gasnet)
      DO_GASNET='0'
      ;;
    # Run "make dist" instead of "make distcheck"
    -no-check)
      DIST='dist'
      ;;
    --)
      shift
      break
      ;;
    *)
      echo "Unknown argument '$1'"
      exit 1
      ;;
  esac
  shift
done
#
if [ $# -ne 1 ]; then
  echo "usage: $(basename $0) [options] branch-or-tag-name"
  exit 1
fi
TAG=$1
shift
if test -z "$TAG" ; then
  echo "Required argument (a git tag or branch name) cannot be empty/blank"
  exit 1
fi
#
set -e
set -x
#
case $(hostname -s) in
  n2001)
    .  /usr/local/pkg/Modules/init/bash
    module unload autotools vapi gm gm2 mpi gmake gcc
    module load autotools/autotools-newest
    module load gmake/newest
    module load gcc/gcc-4.8.0
    module load git
    export TMPDIR=/dev/shm
    ;;
  pcp-d-*)
    export TMPDIR=${XDG_RUNTIME_DIR:-/dev/shm}
    module unload mpi gcc clang
    module load PrgEnv/llvm/4.0.0-gcc640
    module load autotools/newest gmake/newest
    export CC="${CC:-clang}" CXX="${CXX:-clang++} -std=gnu++98"
    ;;
esac
export LANG=C
#
# unconditional mkdirs (to fail early if not "clean")
#
mkdir bld-upcr
mkdir bld-gasnet
mkdir bld-tools
#
# GIT checkouts
#
if [ $DO_UPCR = 1 ] ; then
   git clone --branch=$TAG $REPO_UPCR   upc-runtime
   git clone --branch=$TAG $REPO_GASNET gasnet
elif [ $DO_GASNET = 1 ] ; then
   git clone --branch=$TAG $REPO_GASNET gasnet
fi
if [ $DO_TRANS = 1 ] ; then
   git clone --branch=$TAG $REPO_TRANS  upc-translator
fi
##
## Function to extract version
##
function get_ver {
  local keyword="$1"
  local file=$2
  local version=$(perl -ne 'if (m/'"$keyword"'(\d+\.\d+\.\d+)/) { print "$1\n"; exit 0; }' -- $file)
  if test -z "$version" ; then
    set +x
    echo ERROR: Could not extract version info from $file >&2
    exit 1
  fi
  echo "$version"
}
##
## Function to relocate/repackage tarball as required
##
function do_pkg {
  local blddir=$1
  local pkg=$2
  if test -n "$RC"; then
    basepkg=`basename $pkg $RC`
    tar xfz ${blddir}/${basepkg}.tar.gz
    mv ${basepkg} ${pkg}
    GZIP=--best tar chfz ${pkg}.tar.gz ${pkg}
    rm -rf ${pkg} ${blddir}/${basepkg}.tar.gz
  else
    mv ${blddir}/${pkg}.tar.gz .
  fi
}
###

#############################
if [ "$DO_TRANS" = 1 ]; then
#
# Translator tarball
#
cd ${TOPDIR}/upc-translator
touch open64/osprey1.0/gccfe/gnu/c-parse.[ch]
gmake dist CC="$CC" CXX="$CXX"
cd ${TOPDIR}
TRANS_PKG=berkeley_upc_translator-$(get_ver 'release ' upc-translator/open64/osprey1.0/be/whirl2c/w2c_driver.cxx)$RC
do_pkg upc-translator $TRANS_PKG
###
if test $DIST = 'distcheck'; then
  #
  # Validate nodist-list
  #
  cd ${TOPDIR}/upc-translator
  gmake update-nodist-list CC="$CC" CXX="$CXX"
  if ! git diff --quiet -- nodist-list; then
    set +x
    echo ERROR: translator nodist-list is not up-to-date
    exit 1
  fi
  #
  # Translator install from tarball
  #
  cd ${TOPDIR}
  tar xfz ${TRANS_PKG}.tar.gz
  cd ${TRANS_PKG}
  ./configure
  gmake all
  gmake install PREFIX=${TOPDIR}/trans-inst
  cd ${TOPDIR}
  rm -rf ${TRANS_PKG}
fi
###
fi # [ "$DO_TRANS" = 1 ]
#############################


#############################
if [ "$DO_UPCR" = 1 ]; then
#
export UPCC_FLAGS="${UPCC_FLAGS}${TRANS_FLAG}"
export UPCC_NORC=1
#
# Copy or move gasnet clone
#
if [ "$DO_GASNET" = 1 ]; then
  cp -rp gasnet upc-runtime/
else
  mv gasnet upc-runtime/
fi
#
# Check unBootstrap
#
cd ${TOPDIR}/upc-runtime
./Bootstrap -L
./unBootstrap
for dir in ${TOPDIR}/upc-runtime ${TOPDIR}/upc-runtime/gasnet; do
  cd $dir
  if test 0 -ne `git status --ignored --porcelain | grep -v 'gasnet/$' | wc -l`; then
    set +x
    echo ERROR: Found untracked files after unBootstrap in `basename $dir`:
    git status --ignored --short | grep -v 'gasnet/$'
    exit 1
  fi
done
#
# Runtime tarball
#
cd ${TOPDIR}/upc-runtime
./Bootstrap -L
cd ${TOPDIR}/bld-upcr
../upc-runtime/configure $UPCR_CONF_FLAGS $GASNET_CONF_FLAGS
gmake $DIST DISTCHECK_CONFIGURE_FLAGS="$UPCR_CONF_FLAGS $GASNET_CONF_FLAGS"
cd ${TOPDIR}
UPCR_PKG=berkeley_upc-$(get_ver 'VERSION=' upc-runtime/configure)$RC
do_pkg bld-upcr $UPCR_PKG
###
fi # [ "$DO_UPCR" = 1 ]
#############################


#############################
if [ "$DO_GASNET" = 1 ]; then
###
cp -rp gasnet gasnet-tools
###
if [ "$DO_UPCR" != 1 ]; then
  #
  # Check unBootstrap
  #
  cd ${TOPDIR}/gasnet
  ./Bootstrap
  ./unBootstrap
  if test 0 -ne `git status --ignored --porcelain | wc -l`; then
    set +x
    echo ERROR: Found untracked files after unBootstrap in `basename $dir`:
    git status --ignored --short
    exit 1
  fi
fi
#
# GASNet tarball
#
cd ${TOPDIR}/gasnet
./Bootstrap
cd ${TOPDIR}/bld-gasnet
../gasnet/configure $GASNET_CONF_FLAGS
gmake $DIST DISTCHECK_CONFIGURE_FLAGS="$GASNET_CONF_FLAGS"
cd ${TOPDIR}
GASNET_PKG=GASNet-$(get_ver 'VERSION=' gasnet/configure)$RC
do_pkg bld-gasnet $GASNET_PKG
if [ "$DO_TOOLS" = 1 ]; then
#
# Tools tarball
#
cd ${TOPDIR}/gasnet-tools
./Bootstrap -o
cd ${TOPDIR}/bld-tools
../gasnet-tools/configure
gmake $DIST DISTCHECK_CONFIGURE_FLAGS="$GASNET_CONF_FLAGS"
cd ${TOPDIR}
TOOLS_PKG=GASNet_Tools-$(get_ver 'VERSION=' gasnet-tools/configure)$RC
do_pkg bld-tools $TOOLS_PKG
fi # [ "$DO_TOOLS" = 1 ]
###
fi # [ "$DO_GASNET" = 1 ]
#############################


#
# Report versions
#
set +x
echo
echo 'PLEASE VERIFY THE FOLLOWING'
echo
echo 'Reported git versions:'
for pkg in ${TRANS_PKG} ${UPCR_PKG} ${GASNET_PKG} ${TOOLS_PKG}; do
    echo -ne "    $pkg\n        "
    tar xOfz ${pkg}.tar.gz ${pkg}/version.git
done
if [ $DO_UPCR = 1 ] ; then
  cd ${TOPDIR}/upc-runtime
  echo 'Default translators:'
  echo -ne "    acinclude.m4\n        "
  grep default_translator= acinclude.m4 | grep -o 'http.*cgi'
  echo -ne "    multiconf.conf.in\n        "
  grep ^BUPC_TRANS multiconf.conf.in | grep -o 'http.*cgi'
  cd ${TOPDIR}
fi
