#!/usr/bin/perl
# This mess makes a mirror of upc.lbl.gov suitable for the PGAS booth CD/DVD
# You just want the Berkeley_UPC-SCxy directory at the end.
# The upc* directories are not needed, but can be kept to speed subsequent runs.

use strict;
use File::Find;
use File::Copy;
use File::Basename;
use POSIX qw/strftime/;

my $srcdir = 'upc.lbl.gov';
my $dstdir = 'Berkeley_UPC-SC' . strftime('%y',localtime);

system("wget --no-check-certificate --mirror -X /download,/hypermail,/internal https://upc.lbl.gov") == 0 or die;

# Fetch docs references into the (otherwise ignored) downloads directory.
my @docs = ();
open(DOCS, '<upc.lbl.gov/docs/index.html') || die $!;
while (<DOCS>) {
  if (m@"\.\.(/download/.*?)"@) {
    push @docs, "https://upc.lbl.gov$1";
  }
}
system("wget --no-check-certificate --mirror " . join(' ', @docs)) == 0 or die;

system("rm -rf $dstdir") == 0 or die;

find({ wanted => \&work, no_chdir => 1 }, $srcdir);
sub work()
{
    (my $target = $_) =~ s|$srcdir|$dstdir|o;
    my $base = basename($_);
    if (-d $_) {
	print "Processing directory $_\n";
	mkdir $target || die $!;
    }
    elsif (m@/upcdecl$@ || m@/robots.txt$@) {
	print "Skipping $_\n";
    }
    elsif ($target =~ m@^(.*)\.s?html$@) {
	print "Converting $_...\n";
	&convert($_, "$1.html");
    }
    else {
        copy($_, $target) || die $!;
    }
}

sub convert($$) {
    my ($srcfile, $dstfile) = @_;
    my $depth = ((my $tmp = $srcfile) =~ tr./..);
    my $path = ($depth == 1) ? './' : ('../' x ($depth - 1));
    my $skip = 0;
    open(SRC, "<$srcfile") or die;
    open(DST, ">$dstfile") or die;
    while (<SRC>) {
	# Rename all references to .shtml pages w/ their .html equivalents
        s@\.shtml@.html@g;

	# Rewrite references to non-mirrored subdirs
	s@"/?(download|hypermail|internal)@"https://upc.lbl.gov/\1@g;

	# Make local references relative
	s@(href|src)="([^/]+)/"@\1="./\2/"@ig;
	s@(href|src)="/@\1="$path@igo;

	# Append index.html to bare (local) directory names
	s@((href|src)="\..*?)/"@\1/index.html"@g;

	# Skip over DL stats and web counter (only appear in top-level index.shtml)
	$skip ^= 1 if (/(BEGIN|END) DLSTATS/ || /(Begin|End) Web counter stuff/);

	print DST unless $skip;
    }
}
