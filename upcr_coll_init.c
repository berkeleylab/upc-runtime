/*
 * UPC Collectives initialization over GASNet
 * $Source: bitbucket.org:berkeleylab/upc-runtime.git/upcr_coll_init.c $
 */

#include <upcr.h>
#include <upcr_internal.h>

/*----------------------------------------------------------------------------*/
/* Initialization, global variables */

    void upcri_coll_init(void) {
        /* Nothing to do */
    }
    void _upcri_coll_init_thread(UPCRI_PT_ARG_ALONE) {
        /* No per-thread work */
    }
