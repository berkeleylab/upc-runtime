This document contains instruction for configuring Berkeley UPC on the "HPE Cray
EX" platform (also known as "Shasta").  We assume familiarity with the Berkeley
UPC `INSTALL.TXT` and with general use of the Cray Programming Environment to
compile software.


This release of Berkeley UPC includes initial support for the HPE Cray EX
platform, including both the "Slingshot-10" and "Slingshot-11" network interface
cards (NICs).  When built in a supported configuration, this release passes our
correctness tests.  However, the performance has not yet been tuned on this
platform.

At the time of this writing we've only tested UPCR on HPE Cray EX systems with
AMD CPUs.

We have tested primarily with `PrgEnv-gnu` and `PrgEnv-cray`, which we consider
to be supported.  `PrgEnv-nvidia`, `PrgEnv-amd` and `PrgEnv-intel` are not yet
officially supported.  In the first two cases (nvidia and amd) this is due to
insufficient duration of testing.  However, there are currently no known issues
with either.  The Berkeley UPC maintainers have had no access to `PrgEnv-intel`
on this platform.  If you choose to use any of the unsupported compiler
families, we welcome your reports of success or failure.

Unlike the Cray XC, the HPE Cray EX is *not* treated as a cross-compilation
target when building Berkeley UPC.  However, we strongly advise use of the
vendor's wrapper compilers, `cc` and `CC`.  Additionally, the two NICs require
distinct non-default settings as described below.

To ensure the necessary build prerequisites are present, one should load a
supported `PrgEnv` environment module, plus `libfabric` and `cray-pmi`.  For
example, if using GNU compilers (`gcc` and `g++`);

```bash
$ module load PrgEnv-gnu libfabric cray-pmi
```

The following shows recommended configure options for this platform, to be added
to options given in `INSTALL.TXT`

```
    CC=cc CXX=CC MPI_CC=cc \
    --with-default-network=ofi \
    --with-ofi-provider=<SEE_BELOW> \
    --with-pmi-runcmd='srun -n %N -- %C'
```

There are two NICs options in an HPE Cray EX system, known as "Slingshot-10" and
"Slingshot-11".  They require different libfabric "providers", as indicated by
the `<SEE_BELOW>` placeholder above:  

  + `--with-ofi-provider=verbs` for Slingshot-10.  
    This is a Mellanox ConnectX-5 100Gbps NIC.  
  + `--with-ofi-provider=cxi` for Slingshot-11.  
    This is an HPE 200Gbps NIC  

If you are uncertain of which NIC is used on a given system, please consult the
site-specific documentation or ask the support staff for assistance.

On _some_ systems with multiple Slingshot NICs, one will need to add
`--with-host-detect=hostname`.  This option is recommended only when actually
required.  If your system _does_ require this setting, then you will see a
message at application run time directing you to use this option, or an
environment-based alternative.

If appropriate at your site, you may also wish to customize the command
passed to `--with-pmi-runcmd=...`.  For instance, if not using the Slurm
Workload Manager.
