#!/usr/bin/perl

# deSSI.pl
#
# renames .shtml files .html, and changes any internal links to .shtml files
# into .html links.

use strict;
use File::Find;

die "Usage: $0 <directory_name>\n" unless @ARGV;

my $version = $ENV{'BUPC_VERSION'};
my $topdir = '../..';

die 'Missing \$BUPC_VERSION' unless $version;

find( \&fix_shtml, $ARGV[0]);

# For each .shtml file:  convert links to .shtml files to .html,
# then rename file to .hmtl
# also perform substitution on @VERSION@ and @TOPDIR@
sub fix_shtml
{
    if (/^(.*)\.shtml$/) {
	print "Converting $_...\n";
        `perl -ni -e 's/\\.shtml/.html/g unless /#include/; \
                      s:<!--#echo.*TOPDIR.*-->:$topdir:g; \
                      s/<!--#echo.*VERSION.*-->/$version/g; \
                      s/\\\@VERSION\\\@/$version/g; \
                      print unless /This page last modified/' $_`;
        die $! if $?;
        `mv $1.shtml $1.html`;
        die $! if $?;
    }
}

