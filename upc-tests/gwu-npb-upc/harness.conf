#
# Classes S and W:
# Use default thread count, except btio which gets NP=4
#
BEGIN_DEFAULT_CONFIG
MakeFlags:      NP=$DEFAULT$ CC='$(UPCC) -DBUPC_TEST_HARNESS $(UPCC_STAT_THREADS)$(NP)' CLINK='$(UPCC) -lm $(UPCC_STAT_THREADS)$(NP)' UCC='$CC$ $CFLAGS$ -DBUPC_TEST_HARNESS' HOST_CC='$HOST_CC$ $HOST_CFLAGS$ -DBUPC_TEST_HARNESS' HOST_LD='$HOST_LD$' HOST_LDFLAGS='$HOST_LDFLAGS$' HOST_LIBS='$HOST_LIBS$'
Flags:
Files:          $TESTNAME$.upc
DynamicThreads: $DEFAULT$
StaticThreads:  0
CompileResult:  pass
PassExpr:       Verification\s*=\s*SUCCESSFUL
FailExpr:       ^Failure:|nan|NaN
ExitCode:       0
BuildCmd:       make,notrans
AppArgs:        
TimeLimit:      300
BenchmarkResult: Mop/s total\s+=\s+(\S+)
WarningFilter:  WARNING: Unable to open \.\./config/last_parameters for reading
# bug 3444: clang warns on glibc's inline strcmp when one string is < 3 chars
WarningFilter: (cc_clang || trans_cupc) && nodebug ; .*? warning: array index . is past the end of the array .*?
END_DEFAULT_CONFIG


TestName: cg-S
KnownFailure: run-match ; nodebug ; bug 653 - NPBs known to fail verification due to application bugs
KnownFailure: run-match ; os_cygwin && debug ; bug 3127 - gwu-npc-upc/cg failures on Cygwin debug
#KnownFailure: compile-crash ; os_darwin && cc_gnu && cpu_32 && nodebug && !trans_opt ; Bug 3233 - Apple's llvm-gcc crashing on GWU's CG 

TestName: cg-W
KnownFailure: run-match ; nodebug ; bug 653 - NPBs known to fail verification due to application bugs
KnownFailure: run-match ; os_cygwin && debug ; bug 3127 - gwu-npc-upc/cg failures on Cygwin debug
#KnownFailure: compile-crash ; os_darwin && cc_gnu && cpu_32 && nodebug && !trans_opt ; Bug 3233 - Apple's llvm-gcc crashing on GWU's CG 

TestName: mg-S
#KnownFailure: run-crash ; network_gm && !segment_fast && pthreads ; Bug 2997 - IS, FT and MG failures on gm-conduit w/ SEGEMENT_{LARGE,EVERYTHING}
KnownFailure: run-match ; (_threads >= 16) ; test fails validation for THREADS >= 16

TestName: mg-W
#KnownFailure: run-crash ; network_gm && !segment_fast && pthreads ; Bug 2997 - IS, FT and MG failures on gm-conduit w/ SEGEMENT_{LARGE,EVERYTHING}
KnownFailure: run-match ; (_threads >= 16) ; test fails validation for THREADS >= 16

TestName: ep-S
KnownFailure: compile-failure ; cc_clang && ( os_netbsd || os_netbsdelf ); clang bug id 8943: pow is transformed to exp2 [even] if the target doesn't have exp2
# Cray C warns about "x-1" argument to vranlc(), but code is otherwise correct
WarningFilter:  cc_cray; CC-170 craycc: WARNING .*?

TestName: ep-W
KnownFailure: compile-failure ; cc_clang && ( os_netbsd || os_netbsdelf ); clang bug id 8943: pow is transformed to exp2 [even] if the target doesn't have exp2
# Cray C warns about "x-1" argument to vranlc(), but code is otherwise correct
WarningFilter:  cc_cray; CC-170 craycc: WARNING .*?

TestName: is-S
#KnownFailure: compile-warning ; cc_cray ; bug2804 (Codegen for array-of-struct accesses causes warnings from Cray C)
#KnownFailure: run-crash ; network_gm && !segment_fast && pthreads ; Bug 2997 - IS, FT and MG failures on gm-conduit w/ SEGEMENT_{LARGE,EVERYTHING}

TestName: is-W
#KnownFailure: compile-warning ; cc_cray ; bug2804 (Codegen for array-of-struct accesses causes warnings from Cray C)
#KnownFailure: run-crash ; network_gm && !segment_fast && pthreads ; Bug 2997 - IS, FT and MG failures on gm-conduit w/ SEGEMENT_{LARGE,EVERYTHING}

TestName: ft-S

TestName: ft-W
#KnownFailure: run-match ; network_gm && !segment_fast && pthreads ; Bug 2997 - IS, FT and MG failures on gm-conduit w/ SEGEMENT_{LARGE,EVERYTHING}

# btio must have perfect square number of threads
TestName: btio-S
MakeFlags:      NP=4 # Appended
DynamicThreads: 4
CompileTimeLimit: 4 * $DEFAULT$
RequireFeature: upc_io
BlockSize: 5882 # 2 * BUF_SIZE, where BUF_SIZE is an NP-dependent value in BTIO/npbparams.h
#KnownFailure: compile-failure ; os_linux && cc_pathscale && cpu_mips64el && debug ; Link failure on SiCortex+pathcc+dbg
KnownFailure: run-match ; ; Bug 1508 - btio fails with NaNs
TimeLimit: 0 # Don't run since it always fails

# btio must have perfect square number of threads
TestName: btio-W
MakeFlags:      NP=4 # Appended
DynamicThreads: 4
CompileTimeLimit: 4 * $DEFAULT$
TimeLimit: 5 * $DEFAULT$
RequireFeature: upc_io
BlockSize: 20282 # 2 * BUF_SIZE, where BUF_SIZE is an NP-dependent value in BTIO/npbparams.h
#KnownFailure: compile-failure ; os_linux && cc_pathscale && cpu_mips64el && debug ; Link failure on SiCortex+pathcc+dbg
KnownFailure: run-match ; ; Bug 1508 - btio fails with NaNs
TimeLimit: 0 # Don't run since it always fails

#
# Class A:
# Use 2X the default threads, except btio which uses 16
#
BEGIN_DEFAULT_CONFIG
MakeFlags:      NP='$(shell expr 2 \* $DEFAULT$)' CC='$(UPCC) -DBUPC_TEST_HARNESS $(UPCC_STAT_THREADS)$(NP)' CLINK='$(UPCC) -lm $(UPCC_STAT_THREADS)$(NP)' UCC='$CC$ $CFLAGS$ -DBUPC_TEST_HARNESS' HOST_CC='$HOST_CC$ $HOST_CFLAGS$ -DBUPC_TEST_HARNESS' HOST_LD='$HOST_LD$' HOST_LDFLAGS='$HOST_LDFLAGS$' HOST_LIBS='$HOST_LIBS$'
Flags:
Files:          $TESTNAME$.upc
DynamicThreads: 2 * $DEFAULT$
StaticThreads:  0
CompileResult:  pass
PassExpr:       Verification\s*=\s*SUCCESSFUL
FailExpr:       ^Failure:|nan|NaN
ExitCode:       0
BuildCmd:       make,notrans
AppArgs:        
TimeLimit:      500
BenchmarkResult: Mop/s total\s+=\s+(\S+)
WarningFilter:  WARNING: Unable to open \.\./config/last_parameters for reading
# bug 3444: clang warns on glibc's inline strcmp when one string is < 3 chars
WarningFilter: (cc_clang || trans_cupc) && nodebug ; .*? warning: array index . is past the end of the array .*?
END_DEFAULT_CONFIG

TestName: cg-A
KnownFailure: run-match ; nodebug ; bug 653 - NPBs known to fail verification due to application bugs
KnownFailure: run-match ; os_cygwin && debug ; bug 3127 - gwu-npc-upc/cg failures on Cygwin debug
#KnownFailure: compile-crash ; os_darwin && cc_gnu && cpu_32 && nodebug && !trans_opt ; Bug 3233 - Apple's llvm-gcc crashing on GWU's CG 

TestName: mg-A
# NOTE: threads is 2*default and thus always even, total heap req. is almost 450MB
AppEnv:                     _threads < 4   && runtime_upcr; UPC_SHARED_HEAP_SIZE=222MB
AppEnv: (_threads >= 4) && (_threads < 8 ) && runtime_upcr; UPC_SHARED_HEAP_SIZE=113MB
AppEnv: (_threads >= 8) && (_threads < 16) && runtime_upcr; UPC_SHARED_HEAP_SIZE=59MB
AppEnv:                    _threads >= 16  && runtime_upcr; UPC_SHARED_HEAP_SIZE=30MB
AppEnv: runtime_upcr; UPC_REQUIRE_SHARED_SIZE=yes
#KnownFailure: run-crash ; network_gm && !segment_fast && pthreads ; Bug 2997 - IS, FT and MG failures on gm-conduit w/ SEGEMENT_{LARGE,EVERYTHING}
KnownFailure: run-match ; (_threads >= 16) ; test fails validation for THREADS >= 16
#KnownFailure: run-mem ; network_smp && cpu_32 && (_threads >= 16) ; at 120M/thread test needs >2GB address space

TestName: ep-A
KnownFailure: compile-failure ; cc_clang && ( os_netbsd || os_netbsdelf ); clang bug id 8943: pow is transformed to exp2 [even] if the target doesn't have exp2
# Cray C warns about "x-1" argument to vranlc(), but code is otherwise correct
WarningFilter:  cc_cray; CC-170 craycc: WARNING .*?

TestName: is-A
#KnownFailure: compile-warning ; cc_cray ; bug2804 (Codegen for array-of-struct accesses causes warnings from Cray C)
#KnownFailure: run-crash ; network_gm && !segment_fast && pthreads ; Bug 2997 - IS, FT and MG failures on gm-conduit w/ SEGEMENT_{LARGE,EVERYTHING}

TestName: ft-A
# NOTE: threads is 2*default and thus always even, total heap req. is almost 460MB
AppEnv:                     _threads < 4   && runtime_upcr; UPC_SHARED_HEAP_SIZE=228MB
AppEnv: (_threads >= 4) && (_threads < 8 ) && runtime_upcr; UPC_SHARED_HEAP_SIZE=115MB
AppEnv: (_threads >= 8) && (_threads < 16) && runtime_upcr; UPC_SHARED_HEAP_SIZE=59MB
AppEnv:                    _threads >= 16  && runtime_upcr; UPC_SHARED_HEAP_SIZE=30MB
AppEnv: runtime_upcr; UPC_REQUIRE_SHARED_SIZE=yes
#KnownFailure: run-match ; network_gm && !segment_fast && pthreads ; Bug 2997 - IS, FT and MG failures on gm-conduit w/ SEGEMENT_{LARGE,EVERYTHING}
#KnownFailure: run-mem ; network_smp && cpu_32 && (_threads >= 16) ; at 120M/thread test needs >2GB address space

# Must have perfect square number of threads
TestName: btio-A
MakeFlags:      NP=16 # Appended
DynamicThreads: 16
BlockSize: 104104 # 2 * BUF_SIZE, where BUF_SIZE is an NP-dependent value in BTIO/npbparams.h
CompileTimeLimit: 4 * $DEFAULT$
TimeLimit: 12 * $DEFAULT$
RequireFeature: upc_io
#KnownFailure: compile-failure ; os_linux && cc_pathscale && cpu_mips64el && debug ; Link failure on SiCortex+pathcc+dbg
KnownFailure: run-match ; ; Bug 1508 - btio fails with NaNs
TimeLimit: 0 # Don't run since it always fails
