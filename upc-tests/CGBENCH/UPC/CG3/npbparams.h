/* CLASS = B */
/*
c  This file is generated automatically by the setparams utility.
c  It sets the number of processors and the class of the NPB
c  in this directory. Do not modify it by hand.
*/
#define	NA	75000
#define	NONZER	13
#define	NITER	75
#define	SHIFT	60.0
#define	RCOND	1.0e-1
#define	CONVERTDOUBLE	FALSE
#define COMPILETIME "09 Oct 2003"
#define NPBVERSION "2.3"
#define NPB_CS1 "(none)"
#define NPB_CS2 "(none)"
#define NPB_CS3 "(none)"
#define NPB_CS4 "(none)"
#define NPB_CS5 "(none)"
#define NPB_CS6 "(none)"
#define NPB_CS7 "(none)"
