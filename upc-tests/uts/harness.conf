#
# Per README, the following configurations are recommended.
#     T3L   (~100 million nodes) for 1-8 processors
#     T3XXL (~3 billion nodes)   for 8-128 processors
#     T2WL  (~300 billion nodes) for 128-1024+ processors
#
# We also run a few smaller cases because we have many smaller systems to test
#
# See sample_trees.sh for the data used to construct AppArgs and PassExpr.
#

##
## T1:
##
BEGIN_DEFAULT_CONFIG
MakeFlags:      -f Makefile.harness
Flags:
DynamicThreads: $DEFAULT$
StaticThreads:  0
CompileResult:  pass
TimeLimit:      $DEFAULT$
ExitCode:       0
BuildCmd:       make,notrans
AppArgs:        -v 2 -t 1 -a 3 -d 10 -b 4 -r 19
PassExpr:       Tree size = 4130071
FailExpr:       ERROR
BenchmarkResult: Wallclock time = \d+\.\d+ sec, performance = (\d+) (nodes/sec) \(\d+ nodes/sec per PE\)
WarningFilter: cc_cray && debug; 'CC-3140 craycc: WARNING File = brg_sha1.*?' # due to bswap32()
END_DEFAULT_CONFIG

TestName: uts-upc-t1
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
#KnownFailure: compile-failure ; cc_cray ; Bug 3162 - wrong upc.h with PrgEnv-cray
## We've NOT resolved bug 3163, but our harness configs on Hopper and Edision work around it
#KnownFailure: compile-failure ; cc_cray ; Bug 3163 - OMP-vs-UPC conflict in UTS

TestName: uts-upc-dcq-t1
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
KnownFailure: run-match ; cc_pgi && pthreads ; bug2079 - alloca () stack overflow w/ PGI

TestName: uts-upc-enhanced-t1
TimeLimit: $DEFAULT$ * 2 # Not exactly "enhanced" on many platforms
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only

##
## T4:
##
BEGIN_DEFAULT_CONFIG
MakeFlags:      -f Makefile.harness
Flags:
DynamicThreads: $DEFAULT$
StaticThreads:  0
CompileResult:  pass
TimeLimit:      $DEFAULT$
ExitCode:       0
BuildCmd:       make,notrans
AppArgs:        -v 2 -t 2 -a 0 -d 16 -b 6 -r 1 -q 0.234375 -m 4 -r 1
PassExpr:       Tree size = 4132453
FailExpr:       ERROR
BenchmarkResult: Wallclock time = \d+\.\d+ sec, performance = (\d+) (nodes/sec) \(\d+ nodes/sec per PE\)
WarningFilter: cc_cray && debug; 'CC-3140 craycc: WARNING File = brg_sha1.*?' # due to bswap32()
END_DEFAULT_CONFIG

TestName: uts-upc-t4
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
#KnownFailure: compile-failure ; cc_cray ; Bug 3162 - wrong upc.h with PrgEnv-cray
## We've NOT resolved bug 3163, but our harness configs on Hopper and Edision work around it
#KnownFailure: compile-failure ; cc_cray ; Bug 3163 - OMP-vs-UPC conflict in UTS

TestName: uts-upc-dcq-t4
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
KnownFailure: run-match ; cc_pgi && pthreads ; bug2079 - alloca () stack overflow w/ PGI

TestName: uts-upc-enhanced-t4
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only

##
## T2L:
##
BEGIN_DEFAULT_CONFIG
MakeFlags:      -f Makefile.harness
Flags:
DynamicThreads: $DEFAULT$
StaticThreads:  0
CompileResult:  pass
TimeLimit:      $DEFAULT$ * 5
ExitCode:       0
BuildCmd:       make,notrans
AppArgs:        -v 2 -t 1 -a 2 -d 23 -b 7 -r 220
PassExpr:       Tree size = 96793510
FailExpr:       ERROR
BenchmarkResult: Wallclock time = \d+\.\d+ sec, performance = (\d+) (nodes/sec) \(\d+ nodes/sec per PE\)
WarningFilter: cc_cray && debug; 'CC-3140 craycc: WARNING File = brg_sha1.*?' # due to bswap32()
END_DEFAULT_CONFIG

TestName: uts-upc-t2l
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
#KnownFailure: compile-failure ; cc_cray ; Bug 3162 - wrong upc.h with PrgEnv-cray
## We've NOT resolved bug 3163, but our harness configs on Hopper and Edision work around it
#KnownFailure: compile-failure ; cc_cray ; Bug 3163 - OMP-vs-UPC conflict in UTS

TestName: uts-upc-dcq-t2l
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
KnownFailure: run-match ; cc_pgi && pthreads ; bug2079 - alloca () stack overflow w/ PGI
KnownFailure: run-time ; os_darwin && network_smp && pthreads && debug ; Bug 3872 - TIME failures for uts-upc-dcq-t2l on macOS/smp/pthreads/DEBUG

TestName: uts-upc-enhanced-t2l
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only

##
## T3L:
##
BEGIN_DEFAULT_CONFIG
MakeFlags:      -f Makefile.harness
Flags:
DynamicThreads: $DEFAULT$
StaticThreads:  0
CompileResult:  pass
TimeLimit:      $DEFAULT$ * 5
ExitCode:       0
BuildCmd:       make,notrans
# The -c 100 is for coarser load-balancing than the default chunk size of 20
AppArgs:        -v 2 -t 0 -b 2000 -q 0.200014 -m 5 -r 7 -c 100
PassExpr:       Tree size = 111345631
FailExpr:       ERROR
BenchmarkResult: Wallclock time = \d+\.\d+ sec, performance = (\d+) (nodes/sec) \(\d+ nodes/sec per PE\)
WarningFilter: cc_cray && debug; 'CC-3140 craycc: WARNING File = brg_sha1.*?' # due to bswap32()
END_DEFAULT_CONFIG

TestName: uts-upc-t3l
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
#KnownFailure: compile-failure ; cc_sun || cc_xlc || cc_mips ; Bug 3036 - translator loses enum types for TLD
#KnownFailure: compile-failure ; cc_cray ; Bug 3162 - wrong upc.h with PrgEnv-cray
## We've NOT resolved bug 3163, but our harness configs on Hopper and Edision work around it
#KnownFailure: compile-failure ; cc_cray ; Bug 3163 - OMP-vs-UPC conflict in UTS

TestName: uts-upc-dcq-t3l
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
KnownFailure: run-match ; cc_pgi && pthreads ; bug2079 - alloca () stack overflow w/ PGI
#KnownFailure: compile-failure ; cc_sun || cc_xlc || cc_mips ; Bug 3036 - translator loses enum types for TLD

TestName: uts-upc-enhanced-t3l
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
#KnownFailure: compile-failure ; cc_sun || cc_xlc || cc_mips ; Bug 3036 - translator loses enum types for TLD

##
## T3XXL:
##
BEGIN_DEFAULT_CONFIG
MakeFlags:      -f Makefile.harness
Flags:
DynamicThreads: 32
StaticThreads:  0
CompileResult:  pass
TimeLimit:      $DEFAULT$ * 8
ExitCode:       0
BuildCmd:       make,notrans
# The -c 500 is for coarser load-balancing than the default chunk size of 20
AppArgs:        -v 2 -t 0 -b 2000 -q 0.499995 -m 2 -r 316 -c 500
PassExpr:       Tree size = 2793220501
FailExpr:       ERROR
BenchmarkResult: Wallclock time = \d+\.\d+ sec, performance = (\d+) (nodes/sec) \(\d+ nodes/sec per PE\)
WarningFilter: cc_cray && debug; 'CC-3140 craycc: WARNING File = brg_sha1.*?' # due to bswap32()
END_DEFAULT_CONFIG

TestName: uts-upc-t3xxl
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
#KnownFailure: compile-failure ; cc_sun || cc_xlc || cc_mips ; Bug 3036 - translator loses enum types for TLD
#KnownFailure: compile-failure ; cc_cray ; Bug 3162 - wrong upc.h with PrgEnv-cray
## We've NOT resolved bug 3163, but our harness configs on Hopper and Edision work around it
#KnownFailure: compile-failure ; cc_cray ; Bug 3163 - OMP-vs-UPC conflict in UTS

TestName: uts-upc-dcq-t3xxl
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
KnownFailure: run-match ; cc_pgi && pthreads ; bug2079 - alloca () stack overflow w/ PGI
#KnownFailure: compile-failure ; cc_sun || cc_xlc || cc_mips ; Bug 3036 - translator loses enum types for TLD

TestName: uts-upc-enhanced-t3xxl
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
# Cannot allocate enough memory in presence of PSHM cross-mapping (32 threads * 64MB = 2GB)
# This also applies to non-SMP if run on a single host, but we cannot express that here.
KnownFailure: run-mem ; os_linux && cpu_32 && network_smp && pshm ; ILP32 address space too small for test with PSHM
#KnownFailure: compile-failure ; cc_sun || cc_xlc || cc_mips ; Bug 3036 - translator loses enum types for TLD

##
## T2WL:
##
BEGIN_DEFAULT_CONFIG
# Default steal stack is not large enough for this problem size
MakeFlags:      -f Makefile.harness FLAGS=-DMAXSTACKDEPTH=2000000
Flags:
DynamicThreads: 256
StaticThreads:  0
CompileResult:  pass
TimeLimit:      $DEFAULT$ * 16
ExitCode:       0
BuildCmd:       make,notrans
# The -c 1000 is for coarser load-balancing than the default chunk size of 20
AppArgs:        -v 2 -t 0 -b 2000 -q 0.4999999995 -m 2 -r 559 -c 1000
PassExpr:       Tree size = 295393891003
FailExpr:       ERROR
BenchmarkResult: Wallclock time = \d+\.\d+ sec, performance = (\d+) (nodes/sec) \(\d+ nodes/sec per PE\)
WarningFilter: cc_cray && debug; 'CC-3140 craycc: WARNING File = brg_sha1.*?' # due to bswap32()
END_DEFAULT_CONFIG

TestName: uts-upc-t2wl
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
# Cannot allocate enough memory in presence of PSHM cross-mapping (2GB / 256 = only 8MB per thread)
# This also applies to non-SMP if run on a single host, but we cannot express that here.
KnownFailure: run-mem ; os_linux && cpu_32 && network_smp && pshm ; ILP32 address space too small for test with PSHM
#KnownFailure: compile-failure ; cc_sun || cc_xlc || cc_mips ; Bug 3036 - translator loses enum types for TLD
#KnownFailure: compile-failure ; cc_cray ; Bug 3162 - wrong upc.h with PrgEnv-cray
## We've NOT resolved bug 3163, but our harness configs on Hopper and Edision work around it
#KnownFailure: compile-failure ; cc_cray ; Bug 3163 - OMP-vs-UPC conflict in UTS

TestName: uts-upc-dcq-t2wl
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
KnownFailure: run-match ; cc_pgi && pthreads ; bug2079 - alloca () stack overflow w/ PGI
#KnownFailure: compile-failure ; cc_sun || cc_xlc || cc_mips ; Bug 3036 - translator loses enum types for TLD

TestName: uts-upc-enhanced-t2wl
KnownFailure: run-match ; cc_open64 && debug && pthreads ; Bug 3042 - Open64-specific UTS validation failures, dbg only
# Cannot allocate enough memory in presence of PSHM cross-mapping (2GB / 256 = only 8MB per thread)
# This also applies to non-SMP if run on a single host, but we cannot express that here.
KnownFailure: run-mem ; os_linux && cpu_32 && network_smp && pshm ; ILP32 address space too small for test with PSHM
#KnownFailure: compile-failure ; cc_sun || cc_xlc || cc_mips ; Bug 3036 - translator loses enum types for TLD
