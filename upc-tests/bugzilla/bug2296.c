int array1dim      [2]    =   { 1, 2 }  ;
int array2dim   [1][2]    =  {{ 3, 4 }} ;
int array3dim[2][2][2]    = {{{ 1, 2},{ 3, 4}},
                             {{ 5, 6},{ 7, 8}}}; 
int array4dim[1][2][1][2] ={{{{ 7, 8 }},{{ 9, 10}}}};

int main(void) {
  // Add validation?
  return 0;
}
