#include <upc.h>

#if defined(__GNUC__) && (__GNUC__ >= 11)
  #define attr_dealloc(fn, argi) __attribute__ ((__malloc__ (dealloc, argno)))
#else
  #define attr_dealloc(fn, argi) /*empty*/
#endif

extern void foo_free(void *);
extern void *foo_alloc(void *, size_t) attr_dealloc(foo_free, 1);

int main(void) { return 0; }
